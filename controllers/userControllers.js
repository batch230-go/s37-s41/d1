
const User = require("../models/User.js");
const Course = require("../models/course.js");

const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const course = require("../models/course.js");


module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		/*
			// bcrypt - package for password hashing
			// hashSync - synchronously generate a hash
			// hash - asynchoursly generate a hash
		*/
		// hashing - converts a value to another value
		password: bcrypt.hashSync(reqBody.password, 10),
		/*
			// 10 = salt rounds
			// Salt rounds is proportional to hashing rounds, the higher the salt rounds, the more hashing rounds, the longer it takes to generate an output 
		*/
		mobileNo: reqBody.mobileNo
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}


module.exports.checkEmailExist = (reqBody) =>{
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	})
}


module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false
		}
		else{
			// compareSync is a bcrypt function to compare unhashed password to hashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password); //true or false

			if(isPasswordCorrect){
				// Let's give the user a token to a access features
				return {access: auth.createAccessToken(result)};
			}
			else{
				// If password does not match, else
				return false;

			}
		}
	})
}




/*
// Activity Part
module.exports.getProfile = (reqBody) => {
    return User.findOne(reqBody._id)
    .then((result, error) => {
    if(error){
        return false;
    }
        result.password = "";
        return result;
    })
}
// End of activity
*/

// Answer to activity s38
/*
module.exports.getProfile = (reqBody) => {
	return User.findById(reqBody._id).then((result, err)=> {
		if(err){
			return false;
		}
		else{
			result.password = "******";
			return result;
		}

	})
}
*/

// This part is s41
module.exports.getProfile = (request, response) => {

	// This will contain your decoded token
	const userData = auth.decode(request.headers.authorization);
	console.log(userData);
	return User.findById(userData.id)
	.then(result => {
		result.password = "******";
		response.send(result);
	})
}

/*
// Enroll Feature
module.exports.enroll = async (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	let courseName = await Course.findById(request.body.courseId)
	.then(result => result.name);
	let newData = {
		// User ID and email will be retrieved from the request header
		// Request header contains the user token
		userId: userData.id,
		email: userData.email,
		// Course ID will be retrieved from the request body
		courseId: request.body.courseId,
		courseName: courseName
	}
	console.log(newData);
	let isUserUpdated = await User.findById(newData.userId)
	.then(user => {
		user.enrollments.push({
			courseId: newData.courseId,
			courseName: newData.courseName,
		})
		return user.save()
		.then(result => {
			console.log(result);
			return true;
		})
		.catch(error => {
			console.log(error);
			return false;
		})
	})
	console.log(isUserUpdated);
	let isCourseUpdated = await Course.findById(newData.courseId)
	.then(course => {
		course.enrollees.push({
			userId: newData.userId,
			email: newData.email
		})
		// Mini activity
		// Create a condition that if the slots is already zero, no deduction of slot will happen
		// Else if the slot if negative value, it should make the slot value to zero
		// Minus the slots available by 1
		// course.slots = course.slots - 1;
		course.slots -= 1;
		return course.save()
		.then(result => {
			console.log(result);
			return true;
		})
		.catch(error => {
			console.log(error);
			return false;
		})
	})
	console.log(isCourseUpdated);

	// Ternary operator
	(isUserUpdated == true && isCourseUpdated == true)? 
	response.send(true) : response.send(false);
}
*/

module.exports.enroll = async (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	let courseName = await Course.findById(request.body.courseId)
	.then(result => result.name);
	let newData = {
		// User ID and email will be retrieved from the request header
		// Request header contains the user token
		userId: userData.id,
		email: userData.email,
		// Course ID will be retrieved from the request body
		courseId: request.body.courseId,
		courseName: courseName
	}
	console.log(newData);
	let isUserUpdated = await User.findById(newData.userId)
	.then(user => {
		user.enrollments.push({
			courseId: newData.courseId,
			courseName: newData.courseName,
		})
		return user.save()
		.then(result => {
			console.log(result);
			return true;
		})
		.catch(error => {
			console.log(error);
			return false;
		})
	})
	console.log(isUserUpdated);
	let isCourseUpdated = await Course.findById(newData.courseId)
	.then(course => {
		course.enrollees.push({
			userId: newData.userId,
			email: newData.email
		})
		// Mini activity
		// Create a condition that if the slots is already zero, no deduction of slot will happen
		// Else if the slot if negative value, it should make the slot value to zero
		// Minus the slots available by 1
		// course.slots = course.slots - 1;
		if(course.slots <= 0){
			console.log("No slots available");
			return false;
		}
		else{
			course.slots -= 1;
			return course.save()
			.then(result => {
				console.log(result);
				return true;
			})
			.catch(error => {
				console.log(error);
				return false;
			})
		}
	})
	console.log(isCourseUpdated);

	// Ternary operator
	(isUserUpdated == true && isCourseUpdated == true)? 
	response.send(true) : response.send(false);
}















module.exports.deleteUser = (req, res) => {
	console.log(req.params.id)

	User.findByIdAndRemove(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error));
}




