const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth.js");

router.post("/register", (request, response) => {
	userControllers.registerUser(request.body).then(resultFromController => response.send(resultFromController));
});

router.post("/checkEmail", (request, response) => {
	userControllers.checkEmailExist(request.body).then(resultFromController => response.send(resultFromController))
});

router.post("/login", (request, response) => {
	userControllers.loginUser(request.body).then(resultFromController => response.send(resultFromController))
});

// ACTIVITY Part
/*
router.post("/details", (request, response) => {
	userControllers.getProfile(request.body)
    .then(resultFromController =>  response.send(resultFromController))
});
*/
// End of Activity

// This part is s41
router.get("/details", auth.verify, userControllers.getProfile);
// Enrollment part
router.post("/enroll", auth.verify, userControllers.enroll);

// Added by Me
router.delete("/deleteUser/:id", userControllers.deleteUser);
module.exports = router;